import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private get_headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
  private main_endpoint: string; 

  constructor( private http: HttpClient, private config: ConfigService ) {
    this.main_endpoint = this.generateEndPoint();
  }

  public get(path: string): Observable<HttpResponse<Object>> {
    return this.http.get(`${this.main_endpoint}${path}`, { headers: this.get_headers, observe: 'response' });
  }

  private generateEndPoint(): string {
    return `${this.config.Settings.apiServer.url}${this.config.Settings.apiServer.port ? ':' + this.config.Settings.apiServer.port : ''}${ this.config.Settings.apiServer.path ? '/' + this.config.Settings.apiServer.path : '' }`;
  }
}
