export interface IConfig {
    
    env: {
        name: string;
    };

    logging?: {
        console: boolean;
    }

    apiServer: {
        url: string;
        port: number;
        path?: string;
    };

}