import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IConfig } from './config.model';
import { environment } from 'src/environments/environment';
@Injectable({ providedIn: 'root'})
export class ConfigService {

    private settings: IConfig;
    constructor(private http: HttpClient) {}
    public load() {
        const jsonFile = `assets/config/config.${environment.name}.json`;
        return new Promise<void>((resolve, reject) => {
            this.http.get(jsonFile).toPromise().then((response : IConfig) => {
               this.settings = <IConfig>response;
               console.log('Configuración cargada: ', response);
               resolve();
            }).catch((response: any) => {
               reject(`Could not load file '${jsonFile}': ${JSON.stringify(response)}`);
            });
        });
    }

    get Settings() {
        return this.settings;
    }
}