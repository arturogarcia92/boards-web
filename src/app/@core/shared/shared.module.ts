import { NgModule, ModuleWithProviders } from '@angular/core';
import { ApiService } from './services/api.service';
import { ConfigService } from './config/config.service';

@NgModule({
  declarations: [],
  imports: [
  ],
  providers: [ ApiService, ConfigService ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        { provide: SharedModule },
        { provide: ApiService }
      ]
    };
  }}
