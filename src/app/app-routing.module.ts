import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksComponent } from './@pages/tasks/tasks.component';
import { MissionsComponent } from './@pages/missions/missions.component';
import { BoardsComponent } from './@pages/boards/boards.component';

const routes: Routes = [
  { path: '', redirectTo: 'boards', pathMatch: 'full' },
  { path: 'boards', component: BoardsComponent },
  { path: 'missions/:id', component: MissionsComponent },
  { path: 'tasks/:id', component: TasksComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
