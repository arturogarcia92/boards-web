import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ApiService } from '../../@core/shared/services/api.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BoardsApiService {

  constructor(private apiService: ApiService) {}

  public findAll(): Observable<any> {
    return this.apiService.get('/boards?status=todo&sort=update').pipe(
      map((res: HttpResponse<Object> ) => {
        return res.body;
      }),
      catchError((error: HttpErrorResponse) => {
        console.error(error);
        return throwError('backend error');
      })
    );
  }

}
