import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../../@core/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class MissionsApiService {

  constructor(private apiService: ApiService) {}

  public findMissionsByBoardId(boardId: String): Observable<any> {
    return this.apiService.get(`/missions/${boardId}?status=todo&sort=update`);
  }

}
