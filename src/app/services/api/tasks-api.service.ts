import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TasksApiService {

  constructor(private http: HttpClient) {}

  public findTasks(missionId: String): Observable<any> {
    const headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');
    // console.log(this.http.get(`https://taskifyappservice1.herokuapp.com/tasks/${missionId}/basic-info?status=todo&sort=update`, { headers: headers, observe: 'response' }));
    console.log(`https://taskifyappservice1.herokuapp.com/tasks/${missionId}/basic-info?status=todo&sort=update`);
    return this.http.get(`https://taskifyappservice1.herokuapp.com/tasks/${missionId}/basic-info?status=todo&sort=update`, { headers: headers, observe: 'response' });

  }

}
