import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { MissionsApiService } from '../../services/api/missions-api.service';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.scss']
})
export class MissionsComponent implements OnInit {

  private boardId: string;
  private missions: any[];

  constructor(private route: ActivatedRoute, private missionsApiService: MissionsApiService, private router: Router) {
    this.route.params.subscribe( params => this.boardId = params.id );
  }

  ngOnInit() {
    this.findMissionsByBoardId();
  }

  findMissionsByBoardId() {
    this.missionsApiService.findMissionsByBoardId(this.boardId).subscribe((res) => {
    this.missions = res.body.boardMissions;
    });
  }

  loadTasks(mission: any) {
    this.router.navigate(['tasks', mission.missionId]);
  }
}
