import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TasksApiService} from '../../services/api/tasks-api.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  private missionId: string;
  private tasks: any[];

  constructor (private route: ActivatedRoute, private tasksApiService: TasksApiService) {
    this.route.params.subscribe( params => this.missionId = params.id );
  }

  ngOnInit() {
    this.findTasks();
  }

  findTasks() {
    this.tasksApiService.findTasks(this.missionId).subscribe((res) => {
    this.tasks = res.body.missionTasksBasicInfo;
    console.log(this.tasks);
    });
  }

}
