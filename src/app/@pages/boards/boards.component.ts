import { Component, OnInit, AfterViewInit } from '@angular/core';
import { BoardsApiService } from '../../services/api/boards-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {

  public boards: any[];

  constructor(private boardsApiService: BoardsApiService, private router: Router) { }

  ngOnInit() {
    this.findBoards();
  }

  private findBoards(): void {
    this.boardsApiService.findAll().subscribe((res: any ) => {
      this.boards = res;
    });
  }

  public loadBoard(board: any) {
    this.router.navigate(['missions', board.boardId]);
  }

}
