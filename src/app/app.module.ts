import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TasksComponent } from './@pages/tasks/tasks.component';
import { HttpClientModule } from '@angular/common/http';
import { MissionsComponent } from './@pages/missions/missions.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './@core/shared/shared.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BoardsComponent } from './@pages/boards/boards.component';
import { CardBoardComponent } from './components/cards/card-board/card-board.component';
import { CardMissionComponent } from './components/cards/card-mission/card-mission.component';
import { CardTaskComponent } from './components/cards/card-task/card-task.component';
import { ConfigService } from './@core/shared/config/config.service';
import { ApiService } from './@core/shared/services/api.service';

// DevExtreme 
import { DxDataGridModule } from 'devextreme-angular';

export function initializeApp(configService: ConfigService) {
  return () => configService.load();
}

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    MissionsComponent,
    NavbarComponent,
    BoardsComponent,
    CardBoardComponent,
    CardMissionComponent,
    CardTaskComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    SharedModule.forRoot(),
    DxDataGridModule,
  ],
  providers: [
    ConfigService,
    ApiService,
    { 
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ ConfigService ], multi: true 
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
