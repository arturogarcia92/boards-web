import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card-mission',
  templateUrl: './card-mission.component.html',
  styleUrls: ['./card-mission.component.scss']
})
export class CardMissionComponent implements OnInit {

  @Input('mission') public mission;

  constructor() { }

  ngOnInit() {
  }

}
